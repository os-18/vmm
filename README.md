## QMM

The QMM project is a simple wrapper over the `libvirt` tools for quickly
deploying and performing actions in a QEMU/KVM virtual machine.

This project is distributed in the hope that it will be useful,
but without any warranty; without even the implied warranty of
merchantability or fitness for a particular purpose.

Virtual machine parameters, commands at initialization and
commands to be executed are defined by the file "qmmfile.json".
This file must be in the current directory when `qmm` is running.

Example:

```json
{
  "base": "https://files.os18.tech/qmm/debian12-qmm.qcow2",
  "vm_disk": "../test-vm/debian12.qcow2",
  "name": "debian12-qmm",
  "os": "debian12",
  "cpus": 2,
  "memory": 2048,
  "init_commands": [
    "apt update",
    "apt install -y bc"
  ],
  "commands": {
    "probe": [
      "cat /etc/os-release",
      "echo It's working!"
    ],
    "calculator": [
      "echo '2+2' | bc"
    ]
  }
}
```

---

## Dependencies

This project uses [libvirt](https://libvirt.org).

Installation and configuration features are individual for each Linux distribution.
It is necessary to have a configured 'default' network for virtual machines.

It also requires [jq](https://github.com/jqlang/jq) for the program work.

---

## Ready-made packages

See:  
[Download Page of OS-18](https://codeberg.org/os-18/os-18-docs/src/branch/master/OS-18_Packages.md)  

The page contains information about installing several packages, including QMM.

---

## Installation from sources

```bash
make install
```

The program will be installed to `/usr/local/bin` (`bin` is a directory for
programs, `PREFIX` is `usr/local`).

The Makefile contains variables:

* `DESTDIR` specifies the root directory for installing (empty, by default);
* `PREFIX` points to the base directory like `usr/local` or `usr`.

Installation directory is defined as `$(DESTDIR)/$(PREFIX)` in the Makefile.

You can install the scripts of this project to any other directory,
like `$HOME/.local` (script files will be in `$HOME/.local/bin/`):

```bash
make install DESTDIR=$HOME PREFIX=.local
```

Uninstall:

```bash
make uninstall
```

If you installed the project in an alternative directory:

```bash
make uninstall DESTDIR=$HOME PREFIX=.local
```

---

## Feedback

Questions, suggestions, comments, bugs:

**tech.vindex@gmail.com**

Also use the repository service tools.

