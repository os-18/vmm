#!/bin/bash
OSTYPE=debian12

ROOT_URL="https://files.os18.tech/qmm"
IM_NAME="debian12-qmm.qcow2"
IM_SRC="$ROOT_URL/$IM_NAME"

VM_NAME="debian12-os18-qmm"
FINAL_IM_NAME="${VM_NAME}.qcow2"

SSH_CMD="ssh -o StrictHostKeyChecking=no"

pushd images/
    if [[ ! -e "$IM_NAME" ]]; then
        wget "$IM_SRC"
    fi
    cp "$IM_NAME" "$FINAL_IM_NAME"
    sudo virt-customize --add "$FINAL_IM_NAME" --hostname "$VM_NAME"
    virt-install --name "temp-machine" \
        --memory 1024 --vcpus=2 --cpu=host \
        --disk "$FINAL_IM_NAME",bus=virtio,cache=none,io=native --import \
        --noautoconsole --virt-type=kvm --osinfo "$OSTYPE" \
        --network network=default
popd > /dev/null
Wait_Access_And_Get_IP() {
    echo "Wait..."
    readonly ATTEMPT_LIMIT=20
    FOUND_IP=''
    declare -i COUNTER=0
    while [[ "$FOUND_IP" == "" && $((COUNTER < ATTEMPT_LIMIT)) ]]; do
        FOUND_LINE=$(virsh domifaddr "temp-machine" | head -3 | tail -1)
        FOUND_IP=$(echo "$FOUND_LINE" | awk '{ print $4 }')
        FOUND_IP=${FOUND_IP%/*}
        COUNTER=$((COUNTER+1))
        sleep 2
    done
    if [[ "$FOUND_IP" == "" ]]; then
        echo "IP not found for temp-machine" >&2
        exit 1
    fi

    ssh-keygen -f "$HOME/.ssh/known_hosts" -R "$FOUND_IP"
    $SSH_CMD "root@$FOUND_IP" 'echo "VM is accessed."'
}
Wait_Access_And_Get_IP


Print_Commands() {
    export DIST=Debian_12
    # export MIRR=https://ftp.gwdg.de/pub/opensuse/repositories/home:/OS-18
    export MIRR="https://download.opensuse.org/repositories/home:OS-18"
    export KEY='/etc/apt/trusted.gpg.d/home_OS-18.gpg'
    cat <<EndOfCommands
set +o history
apt update
apt install -y binutils make gcc g++ gdc clang ldc
apt install -y gdb lldb
apt install -y fuse libfuse3-dev python3 python3-pytest gpg
apt install -y tree hexedit valgrind attr

echo "deb $MIRR/$DIST/ /" | tee /etc/apt/sources.list.d/home:OS-18.list
curl -fsSL $MIRR/$DIST/Release.key | gpg --dearmor | tee $KEY > /dev/null
apt update
apt install -y libamalthea-ldc2-dev libamalthea-ldc2-1-dbgsym
apt install -y libamalthea-gdc-dev libamalthea-gdc-1-dbgsym
apt install -y liboxfuse-ldc2-dev liboxfuse-ldc2-0-dbgsym
apt install -y liboxfuse-gdc-dev liboxfuse-gdc-0-dbgsym
apt install -y pkg-config

rm -f /root/virt-sysprep-firstboot.log
echo "cd /qmm" >> /root/bash.bashrc
EndOfCommands

}
Print_Commands

$SSH_CMD "root@$FOUND_IP" bash <<< $(Print_Commands)


virsh destroy temp-machine
virsh undefine temp-machine
