#!/bin/bash
OSTYPE=debian12

ROOT_URL="https://cloud.debian.org/images/cloud/bookworm/latest/"
IM_NAME="debian-12-nocloud-amd64.qcow2"
IM_SRC="$ROOT_URL/$IM_NAME"

VM_NAME="debian12-qmm"
FINAL_IM_NAME="${VM_NAME}.qcow2"

SSH_CMD="ssh -o StrictHostKeyChecking=no"

mkdir -p images/
pushd images/
    if [[ ! -e "$IM_NAME" ]]; then
        wget "$IM_SRC"
    fi
    cp "$IM_NAME" "$FINAL_IM_NAME"
    SSHD_DIR="/etc/ssh/sshd_config.d"
    SSH_ROOT_CONF="${SSHD_DIR}/rootlogin.conf"
    FIRSTCOMMAND='ssh-keygen -A && systemctl restart sshd'
    sudo virt-customize -a "$FINAL_IM_NAME" --mkdir "${SSHD_DIR}"
    sudo virt-customize -a "$FINAL_IM_NAME" --touch "$SSH_ROOT_CONF"
    sudo virt-customize --add "$FINAL_IM_NAME" \
        --append-line "$SSH_ROOT_CONF":"PermitRootLogin yes" \
        --append-line "$SSH_ROOT_CONF":"PermitEmptyPasswords yes" \
        --append-line "$SSH_ROOT_CONF":"PasswordAuthentication yes" \
        --hostname "$VM_NAME" \
        --firstboot-command "$FIRSTCOMMAND" \
        --mkdir /qmm
    sudo virt-customize -a "$FINAL_IM_NAME" --run-command 'apt update'
    sudo virt-customize -a "$FINAL_IM_NAME" --run-command \
        'apt install -y ssh cloud-guest-utils qemu-guest-agent'
    sudo virt-customize -a "$FINAL_IM_NAME" --run-command 'systemctl enable ssh'
popd > /dev/null

pushd images/
    virt-install --name "temp-machine" \
        --memory 1024 --vcpus=2 --cpu=host \
        --disk "$FINAL_IM_NAME",bus=virtio,cache=none,io=native --import \
        --noautoconsole --virt-type=kvm --osinfo "$OSTYPE" \
        --network network=default
popd > /dev/null
Wait_Access_And_Get_IP() {
    echo "Wait..."
    readonly ATTEMPT_LIMIT=20
    FOUND_IP=''
    declare -i COUNTER=0
    while [[ "$FOUND_IP" == "" && $((COUNTER < ATTEMPT_LIMIT)) ]]; do
        FOUND_LINE=$(virsh domifaddr "temp-machine" | head -3 | tail -1)
        FOUND_IP=$(echo "$FOUND_LINE" | awk '{ print $4 }')
        FOUND_IP=${FOUND_IP%/*}
        COUNTER=$((COUNTER+1))
        sleep 2
    done
    if [[ "$FOUND_IP" == "" ]]; then
        echo "IP not found for temp-machine" >&2
        exit 1
    fi

    ssh-keygen -f "$HOME/.ssh/known_hosts" -R "$FOUND_IP"
    $SSH_CMD "root@$FOUND_IP" "chmod 777 /qmm"
    $SSH_CMD "root@$FOUND_IP" 'echo "VM is accessed."'
}
Wait_Access_And_Get_IP

DISK=$(virsh domblklist temp-machine | tail -2 | head -1 | awk '{ print $2 }')
virsh blockresize "temp-machine" "$DISK" 30G
$SSH_CMD "root@$FOUND_IP" "growpart /dev/vda 1"
virsh destroy temp-machine
virsh undefine temp-machine
