# Copying and distribution of this file, with or without modification,
# are permitted in any medium without royalty provided the copyright notice
# and this notice are preserved.
# This file is offered as-is, without any warranty.

PROJECT=qmm

DESTDIR=
PREFIX=usr/local
INST_DIR=$(DESTDIR)/$(PREFIX)
INST_BIN_DIR=$(INST_DIR)/bin

INST_HELPDIR=$(INST_DIR)/share/help
INST_HELPDIR_EN=$(INST_HELPDIR)/en_US/$(PROJECT)
INST_BASHCOMPDIR=$(INST_DIR)/share/bash-completion/completions/

.PHONY: all install uninstall clean

all:
	@echo "All prepared."

install:
	mkdir -p "$(INST_BIN_DIR)/"
	mkdir -p "$(INST_HELPDIR_EN)/"
	mkdir -p "$(INST_BASHCOMPDIR)/"
	cp source/$(PROJECT) "$(INST_BIN_DIR)/"
	cp help/en_US.txt "$(INST_HELPDIR_EN)/help.txt"
	cp source/_$(PROJECT) "$(INST_BASHCOMPDIR)/$(PROJECT)" || true

uninstall:
	rm -f "$(INST_BIN_DIR)/$(PROJECT)"
	rm -f "$(INST_HELPDIR_EN)/help.txt"
	rm -rf "$(INST_BASHCOMPDIR)/$(PROJECT)"

clean:
	@echo "Nothing to do."
