# Changelog

---

## [0.2.5] - 2025-03-05

- Signal processing during command execution with initialization (`qmm cmd -i`).
  Fixed an issue related to the VM not destroying after a fail situation.

---

## [0.2.4] - 2025-02-22

- Increased wait time from 20 seconds to 60 seconds.
- Fixed a bug related to removing a machine after initialization.

---

## [0.2.3] - 2025-02-08

- Fixed the program's behavior when no arguments are provided.
- SSH command fixed: output is now colorized.
- Fixed loop wit attempts to connect to VM.

---

## [0.2.2] - 2024-12-27

- The initialization start and end notifications are not output
  if initialization commands are not defined.
- Other minor fixes.

---

## [0.2.1] - 2024-07-28

- The project has been renamed from 'vmm' to 'qmm'.

---

## [0.2.0] - 2024-07-28

- If any command is not selected, the program terminates with an error.
- Configuration may contain a custom command interpreter.
- Program output style improvements.
- Added a new command 'connect' for connecting with VM for interactive work.

---

## [0.1.0] - 2024-03-10

- Initial version.
